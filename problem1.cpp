#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <iomanip>

using std::vector;
using std::pair;
using std::cout;
using std::cin;
using std::endl;


struct Point {
  int x, y;
  Point(int xx, int yy)
      : x(xx), y(yy) { }
};

double GetDistance(Point point1, Point point2) {
  return sqrt(pow(point1.x - point2.x, 2) + pow(point1.y - point2.y, 2));
}

struct Edge {
  int from, to;
  double weight;
  Edge(int fr, int t, double w) 
      : from(fr), to(t), weight(w) {}

  Edge()
      : from(0), to(0), weight(0) {}
  Edge reverse() const {
    return Edge(to, from, weight);
  }
  double priority() const {
    return weight;
  }
};

struct ReachedNode {
  int vertex, time;
  ReachedNode(int v, int t=-1)
      : vertex(v), time(t) {}
  int priority() const {
    return time;
  }
};

class Graph {
 private:
  vector<vector<Edge>> adjacency_list;
  // vertices are assumed to be numbers from 0 to n-1

 public:
  Graph(int size) {
    adjacency_list = vector<vector<Edge>>(size, vector<Edge>());
  }

  Graph(const vector<vector<Edge>> &adjacency_list_param) {
    adjacency_list = adjacency_list_param;
  }

  const vector<Edge> &operator[](int from_vertex) const {
    return adjacency_list[from_vertex];
  }

  void AddEdge(const Edge &edge) {
    adjacency_list[edge.from].push_back(edge);
    adjacency_list[edge.to].push_back(edge.reverse());
  }

  int size() const {
    return adjacency_list.size();
  }
};


Graph ReadGraph() {
  size_t nodes;
  cin >> nodes;
  Graph g = Graph(nodes);

  vector<Point> points = vector<Point>();
  for (size_t i = 0; i < nodes; i++) {
    int x, y;
    cin >> x >> y;
    points.push_back(Point(x, y));
  }
  for (int i = 0; i < nodes; ++i) {
    for (int j = i + 1; j < nodes; ++j) {
      g.AddEdge(Edge(i, j, GetDistance(points[i], points[j])));
    }
  }
  return g;
}


// Element must implement priority() function
template<class Element>
class MinHeap {
 private:
  vector<Element> elements;
  int Left(int el_index) {
    return el_index * 2 + 1;
  }

  int Right(int el_index) {
    return el_index * 2 + 2;
  }

  bool HasLeft(int el_index) {
    return (Left(el_index) < elements.size());
  }

  bool HasRight(int el_index) {
    return (Right(el_index) < elements.size());
  }

  int Parent(int el_index) {
    return (el_index - 1) / 2;
  }

  int Priority(int index) {
    return elements[index].priority();
  }

  void Swap(int index1, int index2) {
    std::swap(elements[index1], elements[index2]);
  }

  void SiftUp(int index) {
    if (Priority(index) < Priority(Parent(index))) {
      Swap(index, Parent(index));
      SiftUp(Parent(index));
    }
  }

  void Heapify(int index) {
    if (!HasLeft(index)) {
      return;
    }
    if (Priority(Left(index)) < Priority(index)) {
      if (!HasRight(index)) {
        Swap(index, Left(index));
      } else {
        // we have to compare it to the right one as well
        // since we are interested in whether right childs 
        // priority is less than lefts one, we compare it
        // only with the left child
        if (Priority(Right(index)) < Priority(Left(index))) {
          Swap(index, Right(index));
          Heapify(Right(index));
        } else {
          Swap(index, Left(index));
          Heapify(Left(index));
        }
      }
    }
  }

 public:
  MinHeap() {
    elements = vector<Element>();
  }

  void add(Element el) {
    elements.push_back(el);
    SiftUp(elements.size() - 1);
  }

  const Element &top() const {
    return elements[0];
  }

  void pop() {
    elements[0] = elements[elements.size() - 1];
    elements.pop_back();
    Heapify(0);
  }

  void size() const {
    return elements.size();
  }

  bool empty() const {
    return elements.empty();
  }
};

double FindShortestNetworkLength(const Graph &graph) {
  // it's actually a realization of Prim's Algorithm
  vector<double> costs = vector<double>(graph.size(), 10000);
  costs[0] = 0;  // it's a start vertex
  vector<bool> taken = vector<bool>(graph.size(), false);
  for (int i = 0; i < graph.size(); ++i) {
    double min_cost = 10000;
    int min_position = -1;
    for (int j = 0; j < graph.size(); ++j) {
      if (taken[j]) {
        continue;
      }
      if (costs[j] < min_cost) {
        min_position = j;
        min_cost = costs[j];
      }
    }
    taken[min_position] = true;
    // cout << "taking " << min_position << " for cost " << min_cost << endl;
    for (const Edge &edge : graph[min_position]) {
      if (taken[edge.to]) {
        continue;
      }
      costs[edge.to] = std::min(costs[edge.to], edge.weight);
    }
  }
  double s = 0;
  for (double cost : costs) {
    // cout << "adding " << cost << endl;
    s += cost;
  }
  return s;
}


int main() {
  cout << std::fixed;
  cout << std::setprecision(7);
  Graph graph = ReadGraph();
  cout << FindShortestNetworkLength(graph);
}
